## 空安全
+ 类型可以声明为空：`var home: Home? = null`
+ `null`不能被赋给非空类型的变量
+ 对于可空类型，如果希望调用它的成员变量或者成员函数，直接调用会出现编译错误，有三种方法可以调用：
    + 在调用前，需要先检查，因为可能为null 
    + 使用`home?.address`的形式调用，如果`home`为`null`，返回`null`，否则返回`address`内容
    + 使用`home!!.address`的形式调用，如果`home`为null，抛出空指针异常，否则返回`address`内容
+ 类型转换：
```
var a: Long = 1
val aInt: Int? = a as Int  // java.lang.ClassCastException
val aInt: Int? = a as? Int
```