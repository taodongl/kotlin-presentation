## 扩展函数

```
fun main(args: Array<String>) {
    println("Hello, World".allBetterWithBang())
}

fun String.allBetterWithBang(): String {
    return "${this}!"
}
```