## 单例
Java(单线程):
```
public class Singleton {
    private Singleton() {
    }
    private static Singleton instance;
    public static Singleton getInstance() {
        if (instance == null) {
            instance = new Singleton();
        }
 
        return instance;
    }
}
```

Java(多线程):
```
public class Singleton {
    private static Singleton instance = null;
    private Singleton(){
    }
    private synchronized static void createInstance() {
        if (instance == null) {
            instance = new Singleton();
        }
    }
    public static Singleton getInstance() {
        if (instance == null) createInstance();
        return instance;
    }
}
```


Kotlin:
```
object Singleton
```