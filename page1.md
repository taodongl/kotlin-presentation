## Kotlin 简介
* [JetBrains](https://www.jetbrains.com/) 公司在2011年发布
* 名字取自圣彼得堡附近的“`Kotlin`”岛
* 运行在`JVM`上(也支持编译成`JavaScript`和`Native`)
* 与`Java`兼容
* Android的官方语言之一(Android Studio 3.0以后版本提供支持)
* 2017.11.28 发布 1.2.0 版本 