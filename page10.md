## SMART CASTING
+ Java:
```
View childView = this.getChildAt(0);
if (childView instanceof ImageView) {
    ((ImageView)childView).setImageAlpha(0);
}
```

+ Kotlin:
```
val view = this.getChildAt(0)
if (view is ImageView) {
    view.imageAlpha = 0
}
```