const gulp = require('gulp');
const gls = require('gulp-live-server');

gulp.task('server', () => {
    let server = gls.static('.', 8080);
    server.start();
    gulp.watch(['*.md', 'index.html'], (file)=>{
        server.notify.apply(server, [file]);
    });
});

gulp.task('default', ['server']);