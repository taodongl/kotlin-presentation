## CONCISENESS
+ Java:
```
view.setOnClickListener(new OnClickListener() {
    @Override
    public void onClick(View v) {
        // do something
    }
});
```

+ Kotlin:
```
view.setOnClickListener { 
    //do something 
}
```

+ Kotlin:
```
view.setOnClickListener { view: View ->
    //do something with view
}
```